#include "fenetre.h"
#include "ui_fenetre.h"
using namespace std;

Fenetre::Fenetre(Plateau *plateau, Robot *ia)
{
    this->ia = ia;
  
    placement = 0;
    tour = true;

    this->plateau = plateau;

    ui = new Ui::Fenetre;
    ui->setupUi(this);

    QPushButton *quitButton = ui->bouton_Quitter;

    QObject::connect(quitButton, SIGNAL(clicked()), qApp, SLOT(quit()));

    QAction *quitMenu = ui->actionQuitter;

    QObject::connect(quitMenu, SIGNAL(triggered()), qApp, SLOT(quit()));

    QPushButton *placer1 = ui->placer_1;
    QSignalMapper *signalMapper = new QSignalMapper(placer1);
    QObject::connect(placer1, SIGNAL(clicked()), signalMapper , SLOT(map()));
    signalMapper->setMapping(placer1, 1);
    connect(signalMapper, SIGNAL(mapped(int)),this, SLOT(placerPionInit(int)));

    QPushButton *placer2 = ui->placer_2;
    QSignalMapper *signalMapper2 = new QSignalMapper(placer2);
    QObject::connect(placer2, SIGNAL(clicked()), signalMapper2 , SLOT(map()));
    signalMapper2->setMapping(placer2, 2);
    connect(signalMapper2, SIGNAL(mapped(int)),this, SLOT(placerPionInit(int)));

    cases[0][0] = ui->case1;
    cases[0][1] = ui->case2;
    cases[0][2] = ui->case3;
    cases[1][0] = ui->case4;
    cases[1][1] = ui->case5;
    cases[1][2] = ui->case6;
    cases[2][0] = ui->case7;
    cases[2][1] = ui->case8;
    cases[2][2] = ui->case9;

    for(int i = 0; i<3; i++){
      for(int j = 0; j<3; j++){

	cases[i][j]->setStyleSheet("background-color: yellow");

	QPushButton *curcase = cases[i][j];
	QSignalMapper *signalMapper = new QSignalMapper(curcase);
	QObject::connect(curcase, SIGNAL(clicked()), signalMapper , SLOT(map()));
	signalMapper->setMapping(curcase, i+j*10); //on ne peut passer qu'un entier et vu qu'ils ne dépassent pas 9 dans notre cas on les passe comma ça
	connect(signalMapper, SIGNAL(mapped(int)),this, SLOT(placerPion(int)));

      }
    }
}

Fenetre::~Fenetre()
{
    delete ui;
}

void Fenetre::afficherPlateau(){

      QString str;

     for(int i = 0; i<3; i++){
      for(int j = 0; j<3; j++){
	if(plateau->getCase(i,j)->getTuile()){
	  cases[i][j]->setStyleSheet("background-color: lightgreen");
	}
	else{
	  cases[i][j]->setStyleSheet("background-color: white");
	}

	if(plateau->getCase(i,j)->getColor() == 0){
	  cases[i][j]->setText("");
	}
	else if(plateau->getCase(i,j)->getColor() == 1){
	  cases[i][j]->setText(str = "1");
	}
	else{
	  cases[i][j]->setText(str = "2");
	}

      }
    }

    if(placement){

      ui->help_label->setText("Placez votre pion sur une case verte libre");

    }
    else{

      ui->help_label->setText("");

    }

      QString pions_j1 = QString::fromUtf8(std::to_string(3-plateau->nbPion(1)).c_str());
      QString pions_j2 = QString::fromUtf8(std::to_string(3-plateau->nbPion(2)).c_str());

      ui->pions_1->setText(pions_j1);
      ui->pions_2->setText(pions_j2);

}

void Fenetre::placerPionInit(int couleur)
{

  placement=couleur;

    for(int i = 0; i<3; i++){
      for(int j = 0; j<3; j++){

	if(plateau->getCase(i,j)->getColor()==0 && plateau->getCase(i,j)->getTuile()){

	  cases[i][j]->setEnabled(true);

	}

      }
    }

  afficherPlateau();

}

void Fenetre::placerPion(int position)
{

    int i,j;

    i = position%10;

    if(position>=10){
      j = position/10;
    }
    else{
      j=0;
    }
    
        
  if(placement>0){

    plateau->placerPion(placement,i,j);
    placement=0;
    afficherPlateau();
    changerTour();

  }
  else if(placement<0){

    int i2,j2;

    i2 = positionPrec%10;

    if(positionPrec>=10){
      j2 = positionPrec/10;
    }
    else{
      j2=0;
    }
    
    if(deplacement)
      plateau->bougerCasesAuto(i2,j2,i,j);
    else
      plateau->bougerPion(j2,i2,j,i);

    placement=0;

    //plateau->affichePlateau();

    changerTour();

    afficherPlateau();

  }
  else{
    
    
    QMessageBox msgBox;
    msgBox.setInformativeText("Que voulez vous bouger ?");
    QAbstractButton *bougerCase = msgBox.addButton(trUtf8("Case"), QMessageBox::YesRole);
    QAbstractButton *bougerPion = msgBox.addButton(trUtf8("Pion"), QMessageBox::NoRole);
    msgBox.setIcon(QMessageBox::Question);    
    msgBox.exec();
    
    if(msgBox.clickedButton() == bougerCase)
    {
       deplacement = true;
       
	bool adjacent;

	for(int i2 = 0; i2<3; i2++){
	  for(int j2 = 0; j2<3; j2++){
	    
	    adjacent = (abs(i-i2) + abs(j-j2)==1);

	    if(adjacent){

	      cases[i2][j2]->setEnabled(true);

	    }
	    else{
	      
	      cases[i2][j2]->setEnabled(false);
	      
	    }

	  }
	}
       
    }
    else
    {
      deplacement = false;
      
      	for(int i2 = 0; i2<3; i2++){
	  for(int j2 = 0; j2<3; j2++){
	    if(plateau->getCase(i2,j2)->getTuile() && plateau->getCase(i2,j2)->getColor() == 0)
	      cases[i2][j2]->setEnabled(true);
	    else
	      cases[i2][j2]->setEnabled(false);

	  }
	}
      
      
    }

    positionPrec = position;
    placement = -1;
    ui->help_label->setText("Cliquez sur la case où vous voulez vous déplacer");
    



  }






}

void Fenetre::changerTour(){
  
  gagne();

  std::cout << "On change de joueur " << tour << std::endl;

    if(tour){

     ui->placer_1->setEnabled(true);
     ui->placer_2->setEnabled(false);
     ui->label_tour->setText("Joueur 1");


     for(int i = 0; i<3; i++){
	for(int j = 0; j<3; j++){

	  if(plateau->getCase(i,j)->getTuile()){

	    cases[i][j]->setEnabled(true);

	  }
	  else{

	    cases[i][j]->setEnabled(false);

	  }

	}
      }
      
      tour = !tour;

    }

    else{

     ui->placer_2->setEnabled(true);
     ui->placer_1->setEnabled(false);
     ui->label_tour->setText("l'IA réfléchit");

     ia->jouer(plateau);
     
     afficherPlateau();
     
     tour = !tour;
     
     changerTour();

     /*for(int i = 0; i<3; i++){
	for(int j = 0; j<3; j++){

	  if(plateau->getCase(i,j)->getTuile()){

	    cases[i][j]->setEnabled(true);

	  }
	  else{

	    cases[i][j]->setEnabled(false);

	  }

	}
      }*/
     
     

    }

}

void Fenetre::gagne()
{
  
  std::cout << "sdfsdfsdf" << std::endl;
  
  plateau->tour = 1;
  bool gagne = plateau->gagne();
  plateau->tour = 2;
  gagne = plateau->gagne() || gagne;
  
 if(gagne){
   
    QMessageBox msgBox;
    msgBox.setInformativeText("Terminé !");
    msgBox.setIcon(QMessageBox::Warning);    
    msgBox.exec();
   
 }
  
}
