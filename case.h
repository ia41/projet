#ifndef CASE_H
#define CASE_H

#include <iostream>
using namespace std;

class Case
{
public:
Case();
Case(const Case& other);
void afficheCase();
int getColor();
bool getTuile();
void setColor(int);
void setTuile(bool);


    int pion; //0 = pas de pion, 1 = blanc, 2 = noir
    bool tuile; //true = une tuile est présente, false = pas de tuile

};

#endif // CASE_H
