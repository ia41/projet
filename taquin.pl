deplacer(X, Y):-deplacer_horizontal(X, Y).
deplacer(X, Y):-deplacer_vertical(X, Y).

deplacer_horizontal([X, Y, Z],[X2, Y, Z]):-member(0,X), decalage(X, X2).
deplacer_horizontal([X, Y, Z],[X, Y2, Z]):-member(0,Y), decalage(Y, Y2).
deplacer_horizontal([X, Y, Z],[X, Y, Z2]):-member(0,Z), decalage(Z, Z2).

deplacer_vertical([X, Y, Z],[X2, Y2, Z]):-member(0,X), inversion(X, X2, Y, Y2).
deplacer_vertical([X, Y, Z],[X2, Y2, Z]):-member(0,Y), inversion(X, X2, Y, Y2).
deplacer_vertical([X, Y, Z],[X, Y2, Z2]):-member(0,Y), inversion(Y, Y2, Z, Z2).
deplacer_vertical([X, Y, Z],[X, Y2, Z2]):-member(0,Z), inversion(Y, Y2, Z, Z2).

decalage([X, Y, Z], [Y, X, Z]).
decalage([X, Y, Z], [X, Z, Y]).

inversion([X, Y, Z], [X2, Y, Z], [X2, Y2, Z2], [X, Y2, Z2]).
inversion([X, Y, Z], [X, Y2, Z], [X2, Y2, Z2], [X2, Y, Z2]).
inversion([X, Y, Z], [X, Y, Z2], [X2, Y2, Z2], [X2, Y2, Z]).

find_path(F, F, 0).
find_path(I, F, N):-N>0, deplacer(I, X), N1 is N - 1, find_path(X, F, N1).

solve(I, F, N):-find_path(I, F, N), !.