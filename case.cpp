#include "case.h"
#include <iostream>
using namespace std;

Case::Case()
{
  
  pion = 0;
  tuile = false;

}

Case::Case(const Case& other)
{
  pion = other.pion;
  tuile = other.tuile;
}

void Case::afficheCase(){ // si tuile est vraie, affiche l'état de la case (0,1,2)

    if (tuile == true)
        std::cout << pion ;
    else
        std::cout << " " ;
}

int Case::getColor()
{
  return pion;
}

bool Case::getTuile()
{
  return tuile;
}

void Case::setColor(int color)
{
  pion = color;
}

void Case::setTuile(bool etat)
{
  tuile = etat;
}

