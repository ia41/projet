:-set_prolog_stack(global, limit(100 000 000 0000)).
:- set_prolog_stack(trail,  limit(20 000 000 0000)).
:- set_prolog_stack(local,  limit(2 000 000 0000)).
:- use_module(library(clpfd)).

/* 0 = Case vide , 1 = Case Sans pion , 2 = Pion adverse (Min) , 3 = Pion de l'IA (Max) */ 

/* Déplacements horizontaux 1 case */

% deplacer_horizontal(+L1,?L2), L1, L2 des listes 
% Déplace horizontalement une case 
% Exemple : deplacer_horizontal([ [0,2,1],[2,3,1],[[1,1,1]], _, _, _], X). X=[[2,0,1],[2,3,1],[[1,1,1], _, _, _].

deplacer_horizontal([[X, Y, Z], _ ,Old, _],[[X2, Y, Z], _, Old, _]):-member(0,X), decalage(X, X2).
deplacer_horizontal([[X, Y, Z], _, Old, _],[[X, Y2, Z], _, Old, _]):-member(0,Y), decalage(Y, Y2).
deplacer_horizontal([[X, Y, Z], _, Old, _],[[X, Y, Z2], _, Old, _]):-member(0,Z), decalage(Z, Z2).

% decalage(+L1,?L2), L1 et L2 des listes 
% Permet d'intervertir deux cases juxtaposées si l'une d'elle est égale à 0'
% Exemple : decalage([1,0,2],[1,2,0]). Vrai.  

decalage([X, Y, Z], [Y, X, Z]):- X =:= 0.
decalage([X, Y, Z], [Y, X, Z]):- Y =:= 0.
decalage([X, Y, Z], [X, Z, Y]):- Y =:= 0.
decalage([X, Y, Z], [X, Z, Y]):- Z =:= 0.

/* Déplacements horizontaux 2 cases */ 

% deplacer_horizontal2(+L1,?L2), L1 et L2 sont des listes 
% Permet de déplacer deux cases en même temps si ces deux cases sont juxtaposées, l'une d'elle juxtaposées à la case vide, 
% case vide se trouvant sur un bord
% Exemple : deplacer_horizontal2([ [0,2,1],[2,3,1],[[1,1,1]], _, _, _], X). X=[[2,1,0],[2,3,1],[[1,1,1], _, _, _]

deplacer_horizontal2([[X, Y, Z], _, Old, _],[[X2, Y, Z], _, Old, _]):-member(0,X), decalage2(X, X2).
deplacer_horizontal2([[X, Y, Z], _, Old, _],[[X, Y2, Z], _, Old, _]):-member(0,Y), decalage2(Y, Y2).
deplacer_horizontal2([[X, Y, Z], _, Old, _],[[X, Y, Z2], _, Old, _]):-member(0,Z), decalage2(Z, Z2).

% decalage2(+L1,?L2), L1 et L2 sont des listes 
% Permet de décaler deux cases d'une ligne, selon les même règles que deplacer_horizontal2'
% Exemple : decalage2([0,2,2],[2,2,0]). Vrai. 

decalage2([X, Y, Z], [Y, Z, X]):- X =:= 0.
decalage2([X, Y, Z], [Z, X, Y]):- Z =:= 0.

/* Déplacements verticaux 1 case*/

% deplacer_vertical(+L1, ?L2), L1 et L2 sont des listes
% Même principe que pour déplacer horizontalement 
% Exemple : deplacer_vertical([[[1,0,2],[1,3,2],[1,1,1]], _, _, _ ], X), X = ([[[1,3,2],[1,0,2],[1,1,1]], _, _, _ ]).

deplacer_vertical([[X, Y, Z], _, Old, _],[[X2, Y2, Z], _, Old, _]):-member(0,X), inversion(X, X2, Y, Y2).
deplacer_vertical([[X, Y, Z], _, Old, _],[[X2, Y2, Z], _, Old, _]):-member(0,Y), inversion(X, X2, Y, Y2).
deplacer_vertical([[X, Y, Z], _, Old, _],[[X, Y2, Z2], _, Old, _]):-member(0,Y), inversion(Y, Y2, Z, Z2).
deplacer_vertical([[X, Y, Z], _, Old, _],[[X, Y2, Z2], _, Old, _]):-member(0,Z), inversion(Y, Y2, Z, Z2).

% inversion(+L1,?L2,+L3,?L4), L1 et L2 sont des listes
% L1 : Première ligne de départ, L2 : première ligne d'arrivée, L3 : deuxième ligne de départ, L4 : deuxième ligne d'arrivée 
% Même principe que decalage, mais verticalement 
% Exemple : inversion([1,2,3],[0,2,3],[0,1,1],[1,1,1])

inversion([X, Y, Z], 
	  [X2, Y, Z], [X2, Y2, Z2], 
	 	      [X, Y2, Z2]) :-X =:= 0.
	 	      
inversion([X, Y, Z], 
	  [X2, Y, Z], [X2, Y2, Z2], 
	 	      [X, Y2, Z2]) :-X2 =:= 0.
	 	      
inversion([X, Y, Z], [X, Y2, Z], [X2, Y2, Z2], [X2, Y, Z2]) :- Y =:= 0.
inversion([X, Y, Z], [X, Y2, Z], [X2, Y2, Z2], [X2, Y, Z2]) :- Y2 =:= 0.
inversion([X, Y, Z], [X, Y, Z2], [X2, Y2, Z2], [X2, Y2, Z]) :- Z =:=0.
inversion([X, Y, Z], [X, Y, Z2], [X2, Y2, Z2], [X2, Y2, Z]) :- Z2 =:=0.

/* Déplacements Verticaux 2 cases */ 

% deplacer_vertical2(+L1,?L2), L1 et L2 sont des listes 
% Pareil que pour deplacer_horizontal2 mais verticalement
% Exemple : ?-deplacer_vertical2([[[1,0,2],[1,3,2],[1,1,1]], _, _, _ ], X), X = ([[[1,3,2],[1,1,2],[1,0,1]], _, _, _ ]).

deplacer_vertical2([[X, Y, Z], _, Old, _],[[X2,Y2,Z2], _, Old, _]):-member(0,X), inversion2(X, X2, Y, Y2, Z, Z2).
deplacer_vertical2([[X, Y, Z], _, Old, _],[[X2,Y2,Z2], _, Old, _]):-member(0,Z), inversion2(Z, Z2, Y, Y2, X, X2).

% inversion2(+L1,?L2,+L3,?L4,+L5,?L6), L1,L2,L3,L4,L5,L6 sont des listes 
% Même principe que inversion mais pour deux cases comme decalage2
% Exemple : inversion2([0,2,3],[1,2,3],[1,3,2],[3,3,2],[3,1,1],[0,1,1]). Vrai 

inversion2(
	  		[A1,A2,A3], 
	   				[B1,A2,A3], 
	   	       	[B1,B2, B3],
	   	       			[C1,B2,B3], 
	   	       	[C1,C2,C3],
	   	       	                [A1,C2,C3]
	   	       		     
	   	       		     ):- A1 =:= 0.
inversion2(
	  		[A1,A2,A3], 
	   				[A1,B2,A3], 
	   	       	[B1,B2, B3],
	   	       			[B1,C2,B3], 
	   	       	[C1,C2,C3],
	   	       	                [C1,A2,C3]
	   	       		     
	   	       		     ):- A2 =:= 0.
	   	       		     
inversion2(
	  		[A1,A2,A3], 
	   				[A1,A2,B3], 
	   	       	[B1,B2, B3],
	   	       			[B1,B2,C3], 
	   	       	[C1,C2,C3],
	   	       	                [C1,C2,A3]
	   	       		     
	   	       		     ):- A3 =:= 0.

/* Déplacements d'une case */

% deplacer1(+L1,?l2), L1 et L2 sont des lises 
% Réunit les fonctions de déplacements horizontales et verticales 

deplacer1(X, Y):-deplacer_horizontal(X, Y).
deplacer1(X, Y):-deplacer_vertical(X, Y).

/* Déplacements de deux cases */ 

% deplacer2(+L1,?l2), L1 et L2 sont des lises 
% Réunit les fonctions de déplacements horizontales et verticales pour deux cases. 
% Le paramètre Old contient la dernière grille qui a été obtenue en déplaçant deux cases, il est interdit de revenir à cette position 
% juste apres que l'autre joueur ait fait ce mouvement '

deplacer2([X, _, Old, _],[Y, _, X, _]) :- deplacer_horizontal2([X, _, _, _],[Y, _, _, _]), Y\= Old.
deplacer2([X, _, Old, _],[Y, _, X, _]) :- deplacer_vertical2([X, _, _, _],[Y, _, _, _]), Y\= Old. 


/* Compte le nombre d'occurence de X dans la liste */

% count(+L,+X,?Resultat), L est une liste, Pion et Resultat sont des entiers 
% Compte le nombre de pion du joueur X dans une ligne de la grille
% Exemple : ?-count([1,2,2], X, Y). Y = 2.
count([],_,0).
count([X|T],X,Y):- count(T,X,Z), Y is 1+Z.
count([X1|T],X,Z):- X1\=X,count(T,X,Z).

/* Retournes le nombre de pions I dans la matrice */
% nbPion(+Liste,+X,?Resultat), Liste est une liste, Pion et Resultat des entiers 
% Compte le nombre de pion du joueur X dans la grille 
% Exemple : ?-nbPion([[1,0,2],[1,3,2],[1,1,1]], 2, R). R = 2.

nbPion([X,Y,Z],I,N):- count(X,I,N1),  count(Y,I,N2),  count(Z,I,N3), N is (N1+N2+N3). 

/* Insérer un élément à la position I */ 
% ins (+Elem, +List, +Pos, ? Result).
% Insérer Elem dans List à la position Pos
% Exemple : ?-ins(3,[1,2],3,X). X=[1,2,3].

ins(Element, List, Position, Result) :-
    PrefixLength is Position - 1,
    length(Prefix, PrefixLength),
    append(Prefix, Suffix, List),
    append(Prefix, [Element], Temp),
    append(Temp, Suffix, Result).
     
/* Enlève un pion I de la grille */ 

% mettreAUn(+Liste,?Liste2, +Pion), Liste1 et isteL2 sont des listes, Pion un entier.
% Met une valeur à 1 (enlever un pion d'une case'), fonctionne sur une ligne.
% Exemple : ?-mettreAUn([0,1,2]),X,2). X=[0,1,1].

mettreAUn(Liste,L2,Pion):- nth1(Pos,Liste,Pion,L3), ins(1,L3,Pos,L2).

% enleverPion(+L1,?L2,+Pion), L1 et L2 sont des listes, Pion un entier.
% Généralise mettreAUn à toute la grille 
% Exemple : ?-enleverPion([[1,0,2],[1,3,2],[1,1,1]], X,2). 
% 		X=[[1,0,1],[1,3,2],[1,1,1]]

enleverPion([X,Y,Z],[X2,Y,Z], I):- mettreAUn(X,X2,I).
enleverPion([X,Y,Z],[X,Y2,Z], I):- mettreAUn(Y,Y2,I).
enleverPion([X,Y,Z],[X,Y,Z2], I):- mettreAUn(Z,Z2,I).

/* Ajoute un pion I à la grille */ 

% mettreAI(+L1,?L2,+Pion), L1 et L2 sont des listes, Pion un entier.
% Remplace une valeur 0 dans une ligne par une valeur Pion
% Exemple : ?-mettreAI([1,2,0],X,3). X=[1,2,3].

mettreAI(Liste,Resultat,Pion):- nth1(Pos,Liste,1,Interm), ins(Pion,Interm,Pos,Resultat).

% ajouterPion(+L1,?L2,+Pion), L1 et L2 sont des listes, Pion un entier.
% Généralise mettreAI à toute la grille. 
% Exemple : ?-ajouter([[1,0,2],[1,3,2],[1,1,1]], X,2). 
%		    X=[[2,0,2],[1,3,2],[1,1,1]].

ajouterPion([X,Y,Z],[X2,Y,Z], I):- nbPion([X,Y,Z],I,R),R<3,mettreAI(X,X2,I).
ajouterPion([X,Y,Z],[X,Y2,Z], I):- nbPion([X,Y,Z],I,R),R<3,mettreAI(Y,Y2,I).
ajouterPion([X,Y,Z],[X,Y,Z2], I):- nbPion([X,Y,Z],I,R),R<3,mettreAI(Z,Z2,I).
ajouter1Pion([L, I , Old , _],[L2, _, Old, _]):- ajouterPion(L,L2,I).

/* Déplace un pion dans la grille */ 

% deplacerPion(+L1,?L2), L1 et L2 sont des listes 
% déplace un pion du joueur courant vers un espace libre dans la grille 
% Exemple : ?-deplacerPion([[[1,0,2],[1,3,2],[1,1,1]], 2, _, _ ], X).
%		      X = ([[[2,0,1],[1,3,2],[1,1,1]], _, _, _ ]).

deplacerPion([L, I, Old, _],[L2, _, Old, _]):- enleverPion(L,L3,I), ajouterPion(L3,L2,I), L2\=L.


/* Effectuer une action */ 

% bouger(+L1,?L2), L1 et L2 sont des listes. 
% réunit toutes les fonctions permettant de passer d'un état à un autre de la grille'

bouger(L,L2):-deplacer1(L,L2).
bouger(L,L2):-deplacer2(L,L2).
bouger(L,L2):-deplacerPion(L,L2).
bouger(L,L2):-ajouter1Pion(L,L2). 

/* Condition de victoire : 3 pions I alignés */  

% winPos(+P,+L), P est un entier, L est une liste.
% Renvoie vrai si la joueur P a gagné. 
% Exemple : winPos(2,[[2,2,2],[1,1,1],[3,1,1]]). Vrai.

winPos(P, [[X1, X2, X3], [X4, X5, X6], [X7, X8, X9]]) :-
    equal(X1, X2, X3, P) ;    % 1st line
    equal(X4, X5, X6, P) ;    % 2nd line
    equal(X7, X8, X9, P) ;    % 3rd line
    equal(X1, X4, X7, P) ;    % 1st col
    equal(X2, X5, X8, P) ;    % 2nd col
    equal(X3, X6, X9, P) ;    % 3rd col
    equal(X1, X5, X9, P) ;    % 1st diag
    equal(X3, X5, X7, P).     % 2nd diag

equal(X, X, X, X).



/* Vrai si on change de joueur */ 

% nextPlayer(+X1,?X2), avec X1, X2 des entiers.
% Passe la main d'un joueur à lautre'
% Exemple : ?-nextPlayer(2,X). X=3.
nextPlayer(2,3).
nextPlayer(3,2).

/* Fonction utilité */ 

% utility(+L,-R), L est une liste, R une valeur.
% Renvoie la valeur associée à la position 
% Exemple : ?- utility([[[1,0,2],[1,3,2],[1,1,1]], 2, _, play ], X). X = 0.

utility([_,2,_,win],-4).
utility([_,3,_,win],+4).

utility([[[_,_,_],[_,2,_],[_,_,_]],_,_,_],-3).
utility([[[_,_,_],[_,3,_],[_,_,_]],_,_,_],+3).

utility([[[2,_,_],[_,_,_],[_,_,_]],_,_,_],-2).
utility([[[_,_,2],[_,_,_],[_,_,_]],_,_,_],-2).
utility([[[_,_,_],[_,_,_],[2,_,_]],_,_,_],-2).
utility([[[_,_,_],[_,_,_],[_,_,2]],_,_,_],-2).

utility([[[3,_,_],[_,_,_],[_,_,_]],_,_,_],+2).
utility([[[_,_,3],[_,_,_],[_,_,_]],_,_,_],+2).
utility([[[_,_,_],[_,_,_],[3,_,_]],_,_,_],+2).
utility([[[_,_,_],[_,_,_],[_,_,3]],_,_,_],+2).

utility([[[_,2,_],[_,_,_],[_,_,_]],_,_,_],-1).
utility([[[_,_,_],[2,_,_],[_,_,_]],_,_,_],-1).
utility([[[_,_,_],[_,_,2],[_,_,_]],_,_,_],-1).
utility([[[_,_,_],[_,_,_],[_,2,_]],_,_,_],-1).

utility([[[_,3,_],[_,_,_],[_,_,_]],_,_,_],+1).
utility([[[_,_,_],[3,_,_],[_,_,_]],_,_,_],+1).
utility([[[_,_,_],[_,_,3],[_,_,_]],_,_,_],+1).
utility([[[_,_,_],[_,_,_],[_,3,_]],_,_,_],+1).


valeur(Position, Val):- 
	findall(X,utility(Position,X),Result), 
	addition(Result, Val).
	
addition([Item],Item).
addition([Item1,Item2 | R], Total):- 
	X is Item1+Item2,
	addition([X|R ] ,Total). 




/* A qui le tour ? */ 

% min/max_to_move(+L), L est une liste.4
% Renvoie vrai si c'est à min/max de jouer'.
% Exemple : ?-min_to_move([[[1,0,2],[1,3,2],[1,1,1]], 2, _, _ ], X). Vrai. 

min_to_move([_,2,_,_]).
max_to_move([_,2,_,_]).

/* Fonction de transition générale */ 

% move(+L1,?L2), L1 et L2 sont des listes.
% Fonction de transition qui fait passer d'un noeud à l'autre. 
% Exemple : ?- move([[[1,0,2],[1,3,2],[1,1,1]], 2, _, play ], X).
%		X = [[[1,0,2],[1,3,2],[2,1,1]], 2, _, play ].

move([L,X1,Old, play], [L2,X2,Old, play]) :-		
    nextPlayer(X1, X2),
    nbPion(L,2,0),					% Si aucune pion sur la grille, choisir un mouvement au hasard
    nbPion(L,3,0),					 
    ajouter1Pion([L,X1,Old, play], [L2,X2,Old, play]),!.
    

move([L,X1,Old, play], [L2,X2,Old, win]) :-		% Si une position gagante est détectée
    nextPlayer(X1, X2),					% elle est immédiatement choisie. 
    bouger([L,X1,Old, play], [L2,X2,Old, win]),
    winPos(X1, L2).

move([L,X1,Old, play], [L2,X2,Old, play]) :-
    nextPlayer(X1, X2),
    bouger([L,X1,Old, play], [L2,X2,Old, play]).

% search(+L,+D,(?Move,?Value)) avec L, Move, des listes et D, Value des entiers 
% Fonction qui renvoit dans Move le meilleure mouvement par rapport à la situation L et la profondeur donnée.



search(Position,Depth,Move,Value) :- 			% On initialise alpha à -1000
   alpha_beta(Depth,Position,-1000,1000,Move,Value),!.	% et beta à 1000
   
search(Position,Depth,Move,Value) :-			% Si aucune solution n'est trouvée pour Depth'
	D is Depth -1 , 				% on essaye avec une profondeur -1.
	search(Position, D, Move, Value).

% alpha_beta(+D,+L,+A,+B,+L2,+V), avec 0,A,B,V des entiers et L,L2 des listes
% Algorithme alpha_beta qui renvoit le meilleur mouvement par rapport à la situation L et la profondeur donnée. 

alpha_beta(0,Position,_Alpha,_Beta,_NoMove,Value) :- 
   valeur(Position,Value),!.

alpha_beta(D,Position,Alpha,Beta,Move,Value) :- 
   D > 0, 
   findall(X,move(Position,X),[Y|R]),
   Alpha1 is -Beta, % max/min
   Beta1 is -Alpha,
   D1 is D-1, 
   evaluate_and_choose([Y|R],Position,D1,Alpha1,Beta1,nil,(Move,Value)).

% evaluate_and_choose(+ListeMvt, +Position, +D,+A,+B,?R,?BestMove), ListeMvt, Position, R sont des listes et D,A,B, sont des entiers
% BestMove est un couple de (Liste, Entier). 
% Evalue chaque noeuds fils possible et renvoie celui qui a la meilleure valeur associée. 

evaluate_and_choose([Move|Moves],Position,D,Alpha,Beta,Record,BestMove) :-
   alpha_beta(D,Move,Alpha,Beta,_OtherMove,Value),
   Value1 is -Value,
   cutoff(Move,Value1,D,Alpha,Beta,Moves,Position,Record,BestMove).
   
   
evaluate_and_choose([],_Position,_D,Alpha,_Beta,Move,(Move,Alpha)). % Sil n'y a plus de noeuds fils, 
								    % alors le noeud courant est le meilleur noeud'

% cutoff(+L,+V,+D,+A,+B,+L2,+L3,+R,+BestMove) avec L,L2,L3,R des listes, 
% V,D,A,B des entiers et BestMove est un couple de (Liste, Entier).
% Coupure Alpha ou Beta 

cutoff(Move,Value,_D,_Alpha,Beta,_Moves,_Position,_Record,(Move,Value)) :- % Si value >= Beta
   Value >= Beta, !.
   
cutoff(Move,Value,D,Alpha,Beta,Moves,Position,_Record,BestMove) :- % Sinon si Alpha < Value < Beta
   Alpha < Value, Value < Beta, !, 
   evaluate_and_choose(Moves,Position,D,Value,Beta,Move,BestMove). % On réévalue 
   
cutoff(_Move,Value,D,Alpha,Beta,Moves,Position,Record,BestMove) :- % Sinon si Value <= Alpha
   Value =< Alpha, !, 
   evaluate_and_choose(Moves,Position,D,Alpha,Beta,Record,BestMove). % On réévalue

