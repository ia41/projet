#ifndef FENETRE_H
#define FENETRE_H

#include <QMainWindow>
#include <QPushButton>
#include <QSignalMapper>
#include <QMessageBox>

#include "plateau.h"
#include "robot.h"

namespace Ui
{
class Fenetre;
}

class Fenetre : public QMainWindow
{
    Q_OBJECT
public:
Fenetre(Plateau*, Robot*);
~Fenetre();
void afficherPlateau();
void changerTour();

public slots:
  
void placerPion(int position);
void placerPionInit(int couleur);
void gagne();

private:
    Ui::Fenetre* ui;
    QPushButton* cases[3][3];
    Plateau* plateau;
    int placement;
    int positionPrec;
    bool tour;
    Robot* ia;
    bool deplacement;
    
};

#endif // FENETRE_H
