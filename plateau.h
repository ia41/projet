#ifndef PLATEAU_H
#define PLATEAU_H

#include "case.h"

class Plateau
{
public:
Plateau();
Plateau(const Plateau& other);
Case *getCase(int, int);
void affichePlateau();
bool deuxCasesDeplace;
bool placerPion(int, int, int);
int nbPion(int);
int tour; // indique le tour du joueur
int couleurJoueur;
int** conversionJtoIA(int**);
void conversionIAtoJ(int**);
    void bouger1Case(int,int,int,int); // deux premiers attributs = case de départ, deux derners = case d'arrivée
    void bouger2Cases(int,int,int,int,int,int);
    void bougerCasesAuto(int x1, int y1, int x2, int y2);
    Case cases[3][3];
    int casesBougees[2][2];
    void bougerPion(int,int,int,int);
    bool gagne();
    int* rechercheCaseLibre();
    string to_string();
};

#endif // PLATEAU_H
