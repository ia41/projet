#include "plateau.h"
#include "case.h"
#include <stdlib.h>
#include <string.h>

Plateau::Plateau()
{

  tour = 1 ;
  for(int i=0;i<3;i++)
  {
    for(int j=0; j<3; j++)
    {
      cases[i][j].setTuile(true);

    }
  }
  cases[1][1].setTuile(false);
}

Plateau::Plateau(const Plateau& other)
{

}

Case* Plateau::getCase(int i, int j)
{
  Case *out = &cases[i][j];
  return out;

}

void Plateau::affichePlateau(){ // affiche l'état de chaque case sous forme de matrice
    int a = 0 ;
    int b = 0 ;

    for (a=0;a<3;++a){
        for (b=0;b<3;++b){
            this->getCase(a,b)->afficheCase();
            std::cout << " " ;
        }

        std::cout << std::endl ;
    }
    std::cout << std::endl ;
}

bool Plateau::placerPion(int couleur, int i, int j)
{
  if(nbPion(couleur)<3 && getCase(i,j)->getTuile() && getCase(i,j)->getColor() == 0)
  {
    cases[i][j].setColor(couleur);
    return true;
  }
  else
  {
    return false;
  }
}

int Plateau::nbPion(int couleur)
{
  int nb = 0;
  for(int i = 0; i<3; i++)
  {
    for(int j=0; j<3; j++)
    {
      if(getCase(i,j)->getColor() == couleur)
      {
	nb++;
      }
    }
  }
  
  affichePlateau();
  
  return nb;
}


void Plateau::bouger1Case(int y1, int x1, int y2, int x2){

    bool c3 = ( abs(x1-x2) + abs(y1-y2)==1); // on regade si la case à déplacer est adjacente à la case libre.

    if (getCase(y2,x2)->getTuile()==false && c3){

        cases[y1][x1].setTuile(false);
        cases[y2][x2].setTuile(true);
        cases[y2][x2].setColor(cases[y1][x1].getColor());
        cases[y1][x1].setColor(0);

        deuxCasesDeplace = false ;

    }
    else{
        std :: cout << "Cette case n'est pas libre !" << std :: endl ;
    }

}



// 1 = case adjacente à la case libre , 2 = case adjacente à la case 1 et distante de 2 à la case libre , 3 = case libre
void Plateau::bouger2Cases(int y1, int x1, int y2, int x2, int y3, int x3){

  
  
  std::cout << "coucou" << std::endl;

    bool c1 = (getCase(y3,x3)->getTuile()==false) ;
    bool c2 = (x3 == 0 || x3 == 2 || y3 == 0 || y3 == 2); // on vérifie si la case est au bord
    bool c3 = ( abs(x1-x3) + abs(y1-y3)==1); // on regarde si la 1e case est adjacente à la case libre
    bool c4 = ( abs(x2-x3) == 2 && abs(y2-y3) == 0) || ( abs(x2-x3) == 0 && abs(y2-y3)==2) ; // on teste si la distance entre la case 2 et 3 et de 2 horizontalement ou verticalement
    // On regarde si le joueur n'essaye pas de bouger les 2 cases précédemment bougées
    bool c5 = (x1== casesBougees[0][0] && y1==casesBougees[0][1] && x2==casesBougees[1][0] && y2==casesBougees[1][1])  ;

    if (c1 && c2 && c3 && c4){

        try {
            if (deuxCasesDeplace && c5){ // on envoit une erreur si tel est le cas
                throw string("L'autre joueur a déjà déplacé ces deux cases ! ");
            }else{ // sinon on déplace les deux cases


                cases[y3][x3].setTuile(true);
                cases[y2][x2].setTuile(false);

                cases[y3][x3].setColor(cases[y1][x1].getColor());
                cases[y1][x1].setColor(cases[y2][x2].getColor());
                cases[y2][x2].setColor(0);

                deuxCasesDeplace = true ;
                casesBougees[0][0]=x1; // coordonnées à comparer pour la case 1
                casesBougees[0][1]=y1;

                casesBougees[1][0]=x3; // coordonnées à comparer pour la case 2
                casesBougees[1][1]=y3;
             }
     }catch(string const& e){
            cout << e << endl ;
        }
    }
}

void Plateau::bougerCasesAuto(int y1, int x1, int y2, int x2){
   
  bool adjacent = (abs(x1-x2) + abs(y1-y2)==1);
  int x3 = x2;
  int y3 = y2;
  
  if(y1-y2){
    
    if(y1-y2 > 0){
      
      y3=y2-1;
      
    }
    else{
      
      y3=y2+1;
      
    }
    
  }
  else{
    
    if(x1-x2 > 0){
      
      x3=x2-1;
      
    }
    else{
      
      x3=x2+1;
      
    }
    
  }
  
  std::cout << "x1 " << x1 << std::endl;
  std::cout << "y1 " << y1 << std::endl;
  std::cout << "x2 " << x2 << std::endl;
  std::cout << "y2 " << y2 << std::endl;
  std::cout << "x3 " << x3 << std::endl;
  std::cout << "y3 " << y3 << std::endl;
  
  
  if (getCase(y2,x2)->getTuile()==false){ //Il faut juste déplacer une case
    
    bouger1Case(y1,x1,y2,x2);
    
  }
  else if (getCase(y3,x3)->getTuile()==false){ //Il faut déplacer 2 cases
        
    bouger2Cases(y2,x2,y1,x1,y3,x3);
    
  }
  
}


void Plateau::bougerPion(int x1, int y1, int x2, int y2){

    if (getCase(y1,x1)->getColor()==1 && getCase(y2,x2)->getTuile()==true){

        cases[y1][x1].setColor(0);
        cases[y2][x2].setColor(1);

         deuxCasesDeplace = false ;
    }
    else{
        std :: cout << "Ce pion n'est pas à vous !" << std :: endl ;
    }
}

bool Plateau::gagne(){

    int i,j ;
    int cpt ;
    bool c1,c2 ;
    
    

    // recherche horizontale

    for (i=0;i<3;i++){
        for(j=0;j<3;j++){
            if (cases[j][i].getColor()==tour) cpt++;
        }
        if(cpt != 3)
	  cpt=0;
    }

    // recherche verticale
    for (i=0;i<3;i++){
        for(j=0;j<3;j++){
            if (cases[i][j].getColor()==tour) cpt++;
        }
        if(cpt != 3)
	  cpt=0;
    }

    // recherche en diagonale
    c1 = cases[0][0].getColor()==tour && cases[1][1].getColor()==tour && cases[2][2].getColor()==tour ;
    c2 = cases[2][0].getColor()==tour && cases[1][1].getColor()==tour && cases[0][2].getColor()==tour ;

    if (cpt==3 || c1 || c2) return true ;
    

    
    return false;

}

int* Plateau::rechercheCaseLibre(){
    int i, j ;
   // int tab[2] ;

    for (i=0;i<3;i++){
        for (j=0;j<3;j++){
            if (cases[i][j].getTuile()==false){
                int tab[2] = {j,i} ;
                return tab ;
        }
    }
}

}

int** Plateau::conversionJtoIA(int** tab){

    int i, j;
    //tab =(int**) malloc(sizeof(int)*9);
    for(i=0;i<3;i++){
        for(j=0;j<3;j++){
            if (cases[i][j].getTuile()==false){
                (tab[i][j]=0);
            }else if (cases[i][j].getColor()==0){
                tab[i][j]=1;
            }else if (cases[i][j].getColor()==1){
                tab[i][j]=2; // Min
            }else {
                tab[i][j]=3; // Max
            }

        }
    }

    return tab;
}

void Plateau::conversionIAtoJ(int** tab){
    int i, j;

        for(i=0;i<3;i++){
            for(j=0;j<3;j++){
                if (tab[i][j] == 0){
                    cases[i][j].setTuile((false));
		    cases[i][j].setColor(0);
                }else if (tab[i][j] == 1) {
		    cases[i][j].setTuile((true));
                    cases[i][j].setColor(0);
                }else if (tab[i][j]==2){ // Min
		    cases[i][j].setTuile((true));
                    cases[i][j].setColor(1);
                }else if (tab[i][j]==3){ // Max
		    cases[i][j].setTuile((true));
                    cases[i][j].setColor(2);
                }

            }
        }
}

string Plateau::to_string()
{

  string out = "[";
  
  int ** tab =(int**) malloc(sizeof(int*)*3);
  tab[0]=(int*) malloc(sizeof(int)*3);
  tab[1]=(int*) malloc(sizeof(int)*3);
  tab[2]=(int*) malloc(sizeof(int)*3);
  
  conversionJtoIA(tab);
  
  
  for(int i=0;i<3;i++){
    
    out += "[";
    
    for(int j=0;j<3;j++){

	out += std::to_string(tab [i][j]);
	
	if(j<2)
	  out += ",";

    }
    
    out += "]";
    
    if(i<2)
      out += ",";
  
  }
  
  out += "]";
  
  return out;
  
}

