#include "robot.h"

Robot::Robot(char **argv)
{
  char* arguments[] = {argv[0],"-q","-s","force3.pl",NULL};
  engine = new PlEngine(4,arguments);
  requete = new PlTermv(4);
}

void Robot::jouer(Plateau *plateau)
{

  
  
  string plateaustr("[");
  
  plateaustr.append(plateau->to_string());
  plateaustr.append(",3,[[0,0,0],[0,0,0],[0,0,0]],play]");
  
  
  
  const char *plateauchar = plateaustr.c_str();
  
  
  cout << "\t" << plateauchar << endl;
    try
  { 
    requete = new PlTermv(4);
    (*requete)[0] = PlCompound(plateauchar);
    (*requete)[1] = 5;

    PlQuery q("search",*requete);
 
    q.next_solution();

  } catch ( PlException &ex )
  { std::cerr << (char *)ex << std::endl;
  }
  
  string resultat = (string)(*requete)[2];
  
  cout << "\t" << resultat << endl;
  
  int ** tab =(int**) malloc(sizeof(int*)*3);
  tab[0]=(int*) malloc(sizeof(int)*3);
  tab[1]=(int*) malloc(sizeof(int)*3);
  tab[2]=(int*) malloc(sizeof(int)*3);
  
  int i = 0;
  
  for(auto bruh : resultat){
    
    int nombre = (int)(bruh - '0');
    
    if(nombre >= 0 && nombre < 4 and i < 9)
    {
      int a = i/3;
      int b = i%3;
      tab[a][b] = nombre;
      i++;
    }
    
  }
  
  plateau->conversionIAtoJ(tab);
  
  plateau->affichePlateau();
  
}